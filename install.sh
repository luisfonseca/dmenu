#!/bin/sh

git apply -v patches/*
sudo make clean install

#Avoid patching a second time
echo "sudo make clean install" > install.sh
